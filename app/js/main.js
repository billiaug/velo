'use strict';

var angular = require('angular');

// angular modules
require('angular-ui-router');
require('./templates');
require('./controllers/_index');
require('./services/_index');
require('./directives/_index');
require('angular-utils-pagination');
require('angular-loading-bar');
require('ngstorage');
require('angular-material');
require('angular-aria');
require('angular-animate');


// create and bootstrap application
angular.element(document).ready(function() {

    var requires = [
        'ui.router',
        'templates',
        'app.controllers',
        'app.services',
        'app.directives',
        'angularUtils.directives.dirPagination',
        'ngStorage',
        'ngAnimate',
        'ngAria',
        'ngMaterial'

    ];

    // mount on window for testing
    window.app = angular.module('app', requires);

    angular.module('app').constant('AppSettings', require('./constants'));
    angular.module('app').config(require('./on_config'));
    angular.module('app').run(require('./on_run'));

    angular.bootstrap(document, ['app']);

});
