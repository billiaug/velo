'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function OverviewController(BikeService, $localStorage) {

    // ViewModel
    var vm = this;

    vm.title = 'A-velo';

    BikeService.data()
        .then(function(data) {
            var response = data;
            vm.stations = response;
        }, function(response) {
            vm.exception = 'Error';
            return response;
        });

    // Localstore setup
    vm.storage = $localStorage.$default({
        counter: 42
    });




}

controllersModule.controller('OverviewController', OverviewController);
