'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function DetailController(BikeService, $stateParams) {
    // ViewModel
    var vm = this;
    vm.title = 'Detailpage';
    vm.id = $stateParams.id;


    BikeService.data()
        .then(function(data) {
            var response = data;
            vm.stations = response;

            // Get the current station
            // state.id - 1, it's an array ;)
            vm.currentStation = response[$stateParams.id - 1];

            // TODO - refactor to filter to strip the ID of the name
            vm.strippedName = vm.currentStation.name.substr(4);

            // Convert the comma list to an array
            vm.nearbyStations = vm.currentStation.nearbyStations.split(',');

        }, function(response) {
            vm.exception = 'Error';
            return response;
        });

}
controllersModule.controller('DetailController', DetailController);
