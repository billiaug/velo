'use strict';

var AppSettings = {
    appTitle: 'a-velo',
    apiUrl: 'https://www.velo-antwerpen.be/availability_map/getJsonObject',
};

module.exports = AppSettings;
