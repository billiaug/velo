'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
function BikeService($q, $http, AppSettings) {

    var service = {};
    var url = AppSettings.apiUrl;
    var dataList = "";


    service.request = function() {
        var deferred = $q.defer();

        $http({
            url: url,
            method: 'GET'
        })
            .success(function(data) {
                deferred.resolve(data);
            })
            .error(function(err, status) {
                deferred.reject(err, status);
            });

        return deferred.promise;
    };

    service.data = function() {
        if (dataList.length === 0) {
            dataList = service.request();
        }

        return dataList;
    };

    return service;
}

servicesModule.service('BikeService', BikeService);
