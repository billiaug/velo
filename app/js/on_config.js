'use strict';

/**
 * @ngInject
 */
function OnConfig($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider) {

    $locationProvider.html5Mode(true);

    $compileProvider.debugInfoEnabled(false);

    $stateProvider
        .state('stations', {
            url: '/stations',
            controller: 'OverviewController as home',
            templateUrl: 'home.html',
            title: 'Home'
        })
        .state('detail', {
            url: '/station/:id',
            controller: 'DetailController as detail',
            templateUrl: 'detail.html',
            title: 'Detail'
        })

    ;

    $urlRouterProvider.otherwise('/stations');

}

module.exports = OnConfig;
